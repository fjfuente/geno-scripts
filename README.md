# Geno-Scripts

Este contenedor implementa una máquina Centos 7 con Ansible y Boto3
para utilizarla como base en la creación de playbooks que utilicen
recursos de Amazon Web Services.

## Requisitos

* [Docker](https://www.docker.com/get-started)


## Configuración

Se debe crear un archivo llamado `secrets.yml` en la carpeta **keys**. 
Este debe contener los token de acceso a AWS.

```
AWS_ACCESS_KEY_ID=<id_llave_secreta_aws>
AWS_SECRET_ACCESS_KEY=<llave_secreta_aws>
```

### Creación y Uso del Contenedor

Para crear el contenedor, luego de haber configurado el sistema previamente
como se describió antes, se debe ejecutar el comando make.

```
make
```

Esto también funciona si se ejecuta el comando build.

```
make build
```

Una vez el contenedor ha sido creado, para ejecutarlo se debe correr
el siguiente comando:

```
make start
```

Para detener el contenedor se debe ejecutar:

```
make stop
```
