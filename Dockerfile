FROM centos:centos7

RUN yum -y install epel-release && yum clean all
RUN yum -y install python-pip && yum clean all
RUN yum -y install --enablerepo="epel" python-pip && yum clean all

RUN \
	yum -y install \
	ansible \
	python-dev && yum clean all

RUN pip install boto3

CMD tail -f /dev/null
