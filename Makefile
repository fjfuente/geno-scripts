all: build

build:
	docker rm -f geno:latest || true
	docker build --force-rm -t geno:latest .

stop:
	docker stop geno

start:
	docker run --rm -d --name geno \
	-v ${HOME}/.ssh:/root/.ssh \
	-v ${PWD}/scripts:/scripts \
	-v ${HOME}/backups:/home/backups/ \
	--env-file ${PWD}/keys/secrets.yml \
	geno
